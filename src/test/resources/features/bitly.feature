Feature: .Bitly Service
  Demonstrate BDD approach for API testing using bitly service.
  - /v3/user/info
  - /v3/user/link_history
  - /v3/shorten


  @HappyFlow
  Scenario: Get user info
  - Get the user info and validate http status and content
    When the user executes get on /user/info api
    Then the call returns with status code 200
    And the user data contains:
      | full_name      | muszashi                  |
      | member_since   | 1568987333                |
      | has_password   | true                      |
      | verified       | true                      |
      | domain_options | [j.mp, bitly.com, bit.ly] |


  @HappyFlow
  Scenario: Get Link history
  - Get the link history and validate http status and content
    When the user executes get on /user/link_history api
    Then the call returns with status code 200
    And the link history contains 3 results
    And the link history for title 'Bitbucket' data contains:
      | keyword_link   | http://bit.ly/Musza                                           |
      | archived       | false                                                         |
      | aggregate_link | http://bit.ly/2kzk0hq                                         |
      | encoding_user  | EncodingUser(login=muszashi, displayName=, fullName=muszashi) |


  @HappyFlow
  Scenario: Get Link history - With optional filtering
  - Get the link history and validate http status and content
    When the user executes get on /user/link_history api:
      | query | Institutional |
    Then the call returns with status code 200
    And the link history contains 1 results
    And the link history #1 data contains:
      | title          | Institutional advocacy                                        |
      | archived       | false                                                         |
      | aggregate_link | http://bit.ly/2mzgZ11                                         |
      | long_url       | https://www.elsevier.com/librarians/institutional-advocacy    |
      | encoding_user  | EncodingUser(login=muszashi, displayName=, fullName=muszashi) |


  @HappyFlow
  Scenario: Shorten
  - Get the shortened link and validate http status and content
    When the user executes get on /shorten api:
      | longUrl | http://www.index.hu |
    Then the call returns with status code 200
    And the shorten data contains:
      | url      | http://bit.ly/30DVYAQ |
      | hash     | 30DVYAQ               |
      | long_url | http://www.index.hu/  |
      | new_hash | 0                     |


  @HappyFlow
  Scenario Outline: Shorten - with optional domain
  - Get the shortened link and validate http status and content
    When the user executes get on /shorten api:
      | longUrl | <url> |
      | domain  | j.mp  |
    Then the call returns with status code 200
    And the shorten data contains:
      | url      | http://j.mp/2mzgYKv |
      | hash     | 2mzgYKv             |
      | long_url | <url>               |
      | new_hash | 0                   |

    Examples:
      | url                                                        |
      | https://www.elsevier.com/librarians/institutional-advocacy |


  @ErrorFlow
  Scenario: Shorten - without protocol
  - Get the shortened link and validate http status and content
    When the user executes get on /shorten api:
      | longUrl | www.index.hu |
    Then the call returns with status code 500 and status text: INVALID_URI