package com.elsevier.bitly.stepdefs;

import com.elsevier.bitly.api.BitlyApi;
import com.elsevier.bitly.api.QueryParam;
import com.elsevier.bitly.domain.linkhistory.LinkHistoryObject;
import com.elsevier.bitly.domain.shorten.ShortenObject;
import com.elsevier.bitly.domain.user.UserObject;
import com.elsevier.bitly.utils.DataReader;
import io.cucumber.datatable.DataTable;
import io.cucumber.java8.En;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class BitlyStepDefs implements En {

    @Autowired
    private BitlyApi bitlyApi;

    @Autowired
    private DataReader dataReader;

    private ValidatableResponse response;
    private UserObject user;
    private LinkHistoryObject linkHistoryObject;
    private ShortenObject shorten;

    public BitlyStepDefs() {

        When("^the user executes get on (.+) api$", (final String path) -> response = bitlyApi.get(path)
                                                                                              .log()
                                                                                              .all());

        When("^the user executes get on (.+) api:$",
             (final String path, final DataTable dataTable) -> {
                 List<QueryParam> queryParamList = dataTable.asMap(String.class, String.class)
                                                            .entrySet()
                                                            .stream()
                                                            .map(entry -> new QueryParam((String) entry.getKey(), (String) entry.getValue()))
                                                            .collect(Collectors.toList());
                 response = bitlyApi.get(path, queryParamList)
                                    .log()
                                    .all();
             });

        Then("^the call returns with status code (\\d+)$", (final Integer expectedStatusCode) -> {
            if (response != null) {
                response.statusCode(HttpStatus.SC_OK)
                        .body("status_code", Matchers.is(expectedStatusCode));
            } else {
                fail("Response is null");
            }
        });

        Then("^the call returns with status code (\\d+) and status text: (.+)$", (final Integer expectedStatusCode, final String expectedStatusText) -> {
            if (response != null) {
                response.statusCode(HttpStatus.SC_OK)
                        .body("status_code", Matchers.is(expectedStatusCode))
                        .body("status_txt", Matchers.is(expectedStatusText));
            } else {
                fail("Response is null");
            }
        });

        Then("^the user data contains:$", (final DataTable dataTable) -> {
            user = response.extract()
                           .body()
                           .as(UserObject.class);
            dataTable.asMap(String.class, String.class)
                     .forEach((k, v) -> assertEquals(v, dataReader.getData(user.getData(), (String) k)));
        });

        Then("^the link history contains (\\d+) (?:result|results)$", (final Integer expectedResult) -> {
            if (response != null) {
                Assert.assertEquals(expectedResult.intValue(), response.extract().as(LinkHistoryObject.class).getData().getResultCount());
            } else {
                fail("Response is null");
            }
        });

        Then("^the link history for title '(.+)' data contains:$", (final String title, final DataTable dataTable) -> {
            linkHistoryObject = response.extract()
                                        .body()
                                        .as(LinkHistoryObject.class);
            dataTable.asMap(String.class, String.class)
                     .forEach((k, v) -> assertEquals(v, dataReader.getData(linkHistoryObject.getData()
                                                                                            .getLinkHistoryByTitle(title), (String) k)));
        });

        Then("^the link history #(\\d+) data contains:$", (final Integer rowNumber, final DataTable dataTable) -> {
            linkHistoryObject = response.extract()
                                        .body()
                                        .as(LinkHistoryObject.class);
            dataTable.asMap(String.class, String.class)
                     .forEach((k, v) -> assertEquals(v, dataReader.getData(linkHistoryObject.getData()
                                                                                            .getLinkHistoryList()
                                                                                            .get(rowNumber-1), (String) k)));
        });

        Then("^the shorten data contains:$", (final DataTable dataTable) -> {
            shorten = response.extract()
                              .body()
                              .as(ShortenObject.class);
            dataTable.asMap(String.class, String.class)
                     .forEach((k, v) -> assertEquals(v, dataReader.getData(shorten.getData(), (String) k)));
        });
    }

}
