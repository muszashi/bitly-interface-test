package com.elsevier.bitly.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class QueryParam {

    private String paramName;
    private String paramValue;
}
