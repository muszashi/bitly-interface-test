package com.elsevier.bitly.api;

import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Component
public class BitlyApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(BitlyApi.class);
    private static final String APPLICATION_JSON_CONTENT_TYPE = "application/json";
    private static final String BASE_URI = "https://api-ssl.bitly.com";
    private static final int BASE_PORT = 443;
    private static final String BASE_PATH = "/v3";
    private static final String ACCESS_TOKEN = "2af6c08962fde9083c79cec4493ea1cec935cd6b";

    private final RequestSpecification specification;

    public BitlyApi() {
        specification = RestAssured.given()
                                   .log()
                                   .all(true)
                                   .spec(new RequestSpecBuilder().setUrlEncodingEnabled(false)
                                                                 .setPort(BASE_PORT)
                                                                 .setBaseUri(BASE_URI)
                                                                 .setBasePath(BASE_PATH)
                                                                 .build());
    }

    public ValidatableResponse get(final String path) {
        return get(path, null);
    }

    public ValidatableResponse get(final String path, List<QueryParam> queryParams) {
        LOGGER.info("Executing get " + path);
        RequestSpecification specification = defaultSpecification().when()
                                                                   .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                                                   .queryParam("access_token", ACCESS_TOKEN);

        if (isNotEmpty(queryParams)) {
            queryParams.forEach(queryParam -> {
                try {
                    specification.queryParam(queryParam.getParamName(), URLEncoder.encode(queryParam.getParamValue(), StandardCharsets.UTF_8.toString()));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException("Cannot encode url: " + queryParam.getParamValue());
                }
            });
        }

        return specification
                .get(path)
                .then();
    }

    private RequestSpecification defaultSpecification() {
        return RestAssured.given()
                          .spec(specification);
    }
}
