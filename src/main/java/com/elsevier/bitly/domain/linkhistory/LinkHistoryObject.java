package com.elsevier.bitly.domain.linkhistory;

import com.elsevier.bitly.domain.BaseDataObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class LinkHistoryObject extends BaseDataObject {

    @JsonProperty("data")
    private LinkHistoryData data;

}
