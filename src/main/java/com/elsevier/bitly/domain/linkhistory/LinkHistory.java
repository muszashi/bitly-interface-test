package com.elsevier.bitly.domain.linkhistory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.List;

@Getter
public class LinkHistory {

    @JsonProperty("keyword_link")
    private String keywordLink;

    @JsonProperty("has_link_deeplinks")
    private boolean hasLinkDeeplinks;

    private boolean archived;

    @JsonProperty("user_ts")
    private long userTs;

    private String title;

    @JsonProperty("created_at")
    private long createdAt;

    private List<String> tags;

    @JsonProperty("modified_at")
    private long modifiedAt;

    @JsonProperty("campaign_ids")
    private List<String> campaignIds;

    @JsonProperty("private")
    @Getter(AccessLevel.NONE)
    private boolean priv;

    @JsonProperty("aggregate_link")
    private String aggregateLink;

    @JsonProperty("long_url")
    private String longUrl;

    @JsonProperty("client_id")
    private String clientId;

    private String link;

    @JsonProperty("is_domain_deeplink")
    private boolean isDomainDeeplink;

    @JsonProperty("encoding_user")
    private EncodingUser encodingUser;

    public boolean isPrivate() {
        return priv;
    }
}
