package com.elsevier.bitly.domain.linkhistory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

@Getter
public class LinkHistoryData {

    @JsonProperty("link_history")
    private List<LinkHistory> linkHistoryList;

    @JsonProperty("result_count")
    private int resultCount;

    public LinkHistory getLinkHistoryByTitle(final String title) {
        if (CollectionUtils.isEmpty(linkHistoryList)) {
            return null;
        }
        return linkHistoryList.stream()
                              .filter(linkHistory -> linkHistory.getTitle().equals(title))
                              .findFirst()
                              .orElseThrow(() -> new RuntimeException("Link history is not found for title: " + title));
    }

}
