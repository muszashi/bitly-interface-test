package com.elsevier.bitly.domain.linkhistory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class EncodingUser {

    private String login;

    @JsonProperty("display_name")
    private String displayName;

    @JsonProperty("full_name")
    private String fullName;
}
