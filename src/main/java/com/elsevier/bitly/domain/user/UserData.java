package com.elsevier.bitly.domain.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
public class UserData {
    @JsonProperty("enterprise_permissions")
    private List<String> enterprisePermissions;
    private String apiKey;

    @JsonProperty("display_name")
    private String dispplayName;

    @JsonProperty("full_name")
    private String fullName;

    @JsonProperty("member_since")
    private long memberSince;

    @JsonProperty("default_link_privacy")
    private String defaultLinkPrivacy;

    @JsonProperty("has_master")
    private int hasMaster;

    @JsonProperty("share_accounts")
    private List<String> shareAccounts;

    @JsonProperty("branded_short_domains")
    private List<String> brandedShortDomains;

    @JsonProperty("has_password")
    private boolean hasPassword;

    @JsonProperty("custom_short_domain")
    private String customShortDomain;

    private String login;

    @JsonProperty("is_enterprise")
    private boolean isEnterprise;

    @JsonProperty("is_verified")
    private boolean isVerified;

    @JsonProperty("tracking_domains")
    private List<String> trackingDomains;

    @JsonProperty("domain_options")
    private List<String> domainOptions;
}

