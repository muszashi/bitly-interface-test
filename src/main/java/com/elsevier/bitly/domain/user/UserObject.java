package com.elsevier.bitly.domain.user;

import com.elsevier.bitly.domain.BaseDataObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class UserObject extends BaseDataObject {

    @JsonProperty("data")
    private UserData data;
}
