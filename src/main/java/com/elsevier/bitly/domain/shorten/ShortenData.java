package com.elsevier.bitly.domain.shorten;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class ShortenData {

    private String url;

    private String hash;

    @JsonProperty("global_hash")
    private String globalHash;

    @JsonProperty("long_url")
    private String longUrl;

    @JsonProperty("new_hash")
    private int newHash;
}
