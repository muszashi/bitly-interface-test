package com.elsevier.bitly.domain.shorten;

import com.elsevier.bitly.domain.BaseDataObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class ShortenObject extends BaseDataObject {

    @JsonProperty("data")
    private ShortenData data;

}
