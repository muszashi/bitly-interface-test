package com.elsevier.bitly.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class BaseDataObject {

    @JsonProperty("status_code")
    private int statusCode;

    @JsonProperty("status_txt")
    private String statusText;
}
