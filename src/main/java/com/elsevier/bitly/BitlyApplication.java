package com.elsevier.bitly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"com.elsevier.bitly"})
public class BitlyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitlyApplication.class, args);
	}

}
