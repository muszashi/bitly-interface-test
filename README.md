# Demo automation project for Elsevier assignment

Demo bit.ly API test automation project for Elsevier assignment to demonstrate the approach

# Project description
* Service for testing- https://bitly.com
* API documentation  - http://dev.bitly.com/api.html 
* You will need to create an account, fill the information about user and create several links.
* In your autotests you need to check for positive and negative cases. Test cases are to be written Gherkin. Tests should check the following methods:
    * /v3/user/ info
    * /v3/user/link_history
    * /v3/shorten
    
# Limitations
As it is a demo project doesn't contain all tha testcases just demonstrate:
* Happy flow
* Alternative happy flow - with optional paramter
* Error flow testcase

# Further improvement
* Cover most of the relevant testcases
    
# Project/technology description
## Base project setup:
* Java 8
* Spring boot
* Maven
* Cucumber
* RestAssured

## Plugins:
* spring-boot-maven-plugin - Provides Spring boot support for maven
* maven-cucumber-reporting - Create cucumber HTML report
* maven-failsafe-plugin - Execute the integration test

## Version control:
The project is using 
* Git for version control
* BitBucket as repository -> https://bitbucket.org/muszashi/bitly-interface-test

# Test execution:
The test are executed with the following maven command:
`mvn clean install -Pbitly-test`
Alternatively the test can be executed from IDE, running the Runner.java however in this case test report is not generated (other than the basic Cucumber report)

# Test report
Test report are generated and placed into the target folder:
`target/cucumber/cucumber-html-reports`

# Used sources
* Quick setup of SpringBoot project - https://start.spring.io/
* My other assignment project: https://bitbucket.org/muszashi/pettest/src/master/

# Pipeline 
As per basic setting there is automatic maven build on develop and master branch on pipeline
* https://bitbucket.org/muszashi/bitly-interface-test/addon/pipelines/home



